resource "aws_s3_bucket" "cinc_cluster_state" {
  bucket = "${var.servicename}-${var.environment}-cluster-state"
  acl    = "private"
  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm = "AES256"
      }
    }
  }
  tags = merge(
    var.standard_tags,
    {
      Name = "${var.servicename}-${var.environment}-cluster-state"
    },
  )
}


resource "aws_s3_bucket_policy" "cinc_cluster_state" {
  bucket = aws_s3_bucket.cinc_cluster_state.id
  policy = <<POLICY
{
    "Version":"2012-10-17",
    "Statement":[
      {
        "Sid":"AddPerm",
        "Effect":"Allow",
        "Principal": { "AWS": "${aws_iam_role.cinc_cluster.arn}" },
        "Action": [
          "s3:*"
        ],
        "Resource":[
          "${aws_s3_bucket.cinc_cluster_state.arn}/*",
          "${aws_s3_bucket.cinc_cluster_state.arn}"
        ]
      }
    ]
}
POLICY
}


resource "aws_s3_bucket_object" "startup_sh" {
  bucket = aws_s3_bucket.cinc_cluster_state.id
  key    = "${var.servicename}-${var.environment}-startup.sh"
  source = "${path.module}/startup.sh"

  # The filemd5() function is available in Terraform 0.11.12 and later
  # For Terraform 0.11.11 and earlier, use the md5() function and the file() function:
  # etag = "${md5(file("path/to/file"))}"
  etag = filemd5("${path.module}/startup.sh")
}
