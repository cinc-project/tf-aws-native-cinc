# This is disabled by default but can be enabled in the future

data "template_file" "pushjobs_userdata" {
  count    = var.pushjobs_enabled == true ? 1 : 0
  template = file("${path.module}/pushjobs_userdata.txt")

  vars = {
    server_name_advertised = aws_route53_record.pushjobs[0].fqdn
  }
}

resource "aws_security_group_rule" "pushjobs_clients_to_elb" {
  count             = var.pushjobs_enabled == true ? 1 : 0
  type              = "ingress"
  protocol          = "TCP"
  cidr_blocks       = var.clientranges
  from_port         = 10000
  to_port           = 10003
  security_group_id = aws_security_group.elb.id
}

resource "aws_security_group_rule" "pushjobs_elb_to_cinc_server" {
  count             = var.pushjobs_enabled == true ? 1 : 0
  type              = "ingress"
  protocol          = "TCP"
  from_port         = 10000
  to_port           = 10003
  cidr_blocks       = [var.cidr]
  security_group_id = aws_security_group.cinc_cluster.id
}

resource "aws_elb" "pushjobs" {
  count           = var.pushjobs_enabled == true ? 1 : 0
  name            = "${var.servicename}-${var.environment}-pushjobs"
  security_groups = [aws_security_group.elb.id]
  subnets         = [aws_subnet.public_az1.id, aws_subnet.public_az2.id, aws_subnet.public_az3.id]

  listener {
    instance_port     = 10000
    instance_protocol = "TCP"
    lb_port           = 10000
    lb_protocol       = "TCP"
  }

  listener {
    instance_port     = 10002
    instance_protocol = "TCP"
    lb_port           = 10002
    lb_protocol       = "TCP"
  }

  listener {
    instance_port     = 10003
    instance_protocol = "TCP"
    lb_port           = 10003
    lb_protocol       = "TCP"
  }

  health_check {
    healthy_threshold   = 2
    unhealthy_threshold = 2
    timeout             = 3
    target              = "TCP:10002"
    interval            = 30
  }

  cross_zone_load_balancing   = true
  idle_timeout                = 400
  connection_draining         = true
  connection_draining_timeout = 400

  tags = merge(
    var.standard_tags,
    {
      Name = "${var.servicename}-${var.environment}"
    },
  )

}

resource "aws_lb_listener_rule" "pushjobs" {
  count        = var.pushjobs_enabled == true ? 1 : 0
  listener_arn = aws_lb_listener.cinc.arn

  action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.cinc_bootstrap.arn
  }

  condition {
    path_pattern {
      values = ["/organizations/*/pushy/*"]
    }
  }
}

resource "aws_autoscaling_attachment" "pushjobs" {
  count                  = var.pushjobs_enabled == true ? 1 : 0
  autoscaling_group_name = aws_autoscaling_group.cinc_bootstrap.id
  elb                    = aws_elb.pushjobs[0].id
}

resource "aws_route53_record" "pushjobs" {
  count   = var.pushjobs_enabled == true ? 1 : 0
  zone_id = join("", data.aws_route53_zone.selected.*.zone_id)
  name    = "${var.servicename}-pushjobs"
  type    = "CNAME"
  ttl     = 300
  records = [aws_elb.pushjobs[0].dns_name]
}

locals {
  pushjobs_hostname = var.pushjobs_enabled == true ? aws_route53_record.pushjobs[0].fqdn : "==DISABLED=="
}
