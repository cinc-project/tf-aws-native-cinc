# Bootstrap node and Pushjobs Server
# this is the first server that should run and MUST  include bootstrap in the name
# so that it copies up the relevant files to the shared cinc state bucket
resource "aws_autoscaling_group" "cinc_bootstrap" {
  name                      = "bootstrap-${aws_launch_configuration.cinc_cluster.name}"
  max_size                  = 3 # to allow replacements
  min_size                  = 1
  desired_capacity          = 1
  health_check_grace_period = 900
  health_check_type         = "EC2"
  vpc_zone_identifier       = [aws_subnet.public_az1.id, aws_subnet.public_az2.id, aws_subnet.public_az3.id]
  launch_configuration      = aws_launch_configuration.cinc_cluster.name
  target_group_arns         = [aws_lb_target_group.cinc_bootstrap.arn, aws_lb_target_group.cinc_nodes.arn]
  enabled_metrics = [
    "GroupDesiredCapacity",
    "GroupInServiceInstances",
    "GroupMaxSize",
    "GroupMinSize",
    "GroupPendingInstances",
    "GroupStandbyInstances",
    "GroupTerminatingInstances",
    "GroupTotalInstances",
  ]

  lifecycle {
    create_before_destroy = true
  }

  tag {
    key                 = "Name"
    value               = "${var.servicename}-${var.environment}-bootstrap"
    propagate_at_launch = true
  }

  dynamic "tag" {
    for_each = var.standard_tags

    content {
      key                 = tag.key
      value               = tag.value
      propagate_at_launch = true
    }
  }
}


# Load-based scaling for these - we have to make sure they come up AFTER the bootstrap
# These servers must never have the name 'bootstrap' in the name.
# via main.sh these servers will replicate a copy of the /etc/opscode repo from s3
resource "aws_autoscaling_group" "cinc_nodes" {
  name                      = "nodes-${aws_launch_configuration.cinc_cluster.name}"
  max_size                  = var.cinc_nodes_max_size
  min_size                  = var.cinc_nodes_min_size
  desired_capacity          = var.cinc_nodes_desired_capacity
  health_check_grace_period = 900
  health_check_type         = "ELB"
  vpc_zone_identifier       = [aws_subnet.public_az1.id, aws_subnet.public_az2.id, aws_subnet.public_az3.id]
  launch_configuration      = aws_launch_configuration.cinc_cluster.name
  target_group_arns         = [aws_lb_target_group.cinc_nodes.arn]
  enabled_metrics = [
    "GroupDesiredCapacity",
    "GroupInServiceInstances",
    "GroupMaxSize",
    "GroupMinSize",
    "GroupPendingInstances",
    "GroupStandbyInstances",
    "GroupTerminatingInstances",
    "GroupTotalInstances",
  ]

  lifecycle {
    ignore_changes        = [desired_capacity]
    create_before_destroy = true
  }

  tag {
    key                 = "Name"
    value               = "${var.servicename}-${var.environment}-nodes"
    propagate_at_launch = true
  }

  dynamic "tag" {
    for_each = var.standard_tags

    content {
      key                 = tag.key
      value               = tag.value
      propagate_at_launch = true
    }
  }

  depends_on = [aws_autoscaling_group.cinc_bootstrap]
}
