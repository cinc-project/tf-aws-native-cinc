#!/bin/bash -x
# top level shell script used to support cinc startup in a clustered mode
# this shell script tries to make the correct guess about bootstraping via the node
# or just initialising a non-bootstrap type node
# based on main.sh of from @ https://github.com/chef-customers/aws_native_chef_server/

echo "[INFO] startup.sh : Startup Checks.."

SOMETHINGWRONG="NO"

# Provided variables that are required: STACKNAME, BUCKET, AWS_REGION
test -n "${STACKNAME}" || export SOMETHINGWRONG="YES"
test -n "${BUCKET}" || export SOMETHINGWRONG="YES"
test -n "${AWS_REGION}" || export SOMETHINGWRONG="YES"

# Check for several utilities we depend on
test -f `which cinc-server-ctl` || export SOMETHINGWRONG="YES"
test -f `which aws` || export SOMETHINGWRONG="YES"
test -f `which curl` || export SOMETHINGWRONG="YES"

if [ $SOMETHINGWRONG == "YES" ] ; then
  echo "[INFO] startup.sh : Startup Checks Failed - Investigate"
  exit 1
else
  echo "[INFO] startup.sh : Startup Checks Passed"
fi

# Determine if we are the bootstrap node
# BOOTSTRAP_TAGS will be empty if not
INSTANCE_ID=`curl -s http://169.254.169.254/latest/meta-data/instance-id`
BOOTSTRAP_TAGS=`aws ec2 describe-tags --region $AWS_REGION --filter "Name=resource-id,Values=$INSTANCE_ID" --output=text | grep bootstrap`

# Check if the configs already exist in S3 - a sign that bootstrap already successfully happened once
CONFIGS_EXIST=`aws s3 ls s3://${BUCKET}/${STACKNAME}/ | grep etc_opscode`

function wait_for_config () {
  echo "[INFO] startup.sh : Cinc Node - waiting for config from bootstrap to be published to s3"
  while ! aws s3 ls s3://${BUCKET}/${STACKNAME}/ | grep migration-level
  do
    echo "migration level config not found, retrying in 10"
    sleep 10
  done
}

function download_config () {
  echo "[INFO] startup.sh : Downloading Config"
  aws s3 sync s3://${BUCKET}/${STACKNAME}/etc_opscode /etc/opscode --exclude "chef-server.rb"
  mkdir -p /var/opt/opscode/upgrades
  touch /var/opt/opscode/bootstrapped
  aws s3 ls s3://${BUCKET}/${STACKNAME}/migration-level && aws s3 cp s3://${BUCKET}/${STACKNAME}/migration-level /var/opt/opscode/upgrades/
}

function upload_config () {
  echo "[INFO] startup.sh : Uploading Config if a migration level is found"
  grep major /var/opt/opscode/upgrades/migration-level || echo "[ERROR] startup.sh : No Migration Level Found - SOMETHING WENT WRONG"
  grep major /var/opt/opscode/upgrades/migration-level && aws s3 sync /etc/opscode s3://${BUCKET}/${STACKNAME}/etc_opscode
  grep major /var/opt/opscode/upgrades/migration-level && aws s3 cp /var/opt/opscode/upgrades/migration-level s3://${BUCKET}/${STACKNAME}/
}

function server_reconfigure () {
  cinc-server-ctl reconfigure
}

function node_start () {
  cinc-server-ctl reconfigure
  cinc-server-ctl start
}

function bootstrap_upgrade_and_start () {
  echo "[INFO] startup.sh : Upgrading CINC Server if a migration level is found"
  grep major /var/opt/opscode/upgrades/migration-level || echo "[ERROR] startup.sh : No Migration Level Found - SOMETHING WENT WRONG"
  grep major /var/opt/opscode/upgrades/migration-level && cinc-server-ctl reconfigure
  grep major /var/opt/opscode/upgrades/migration-level && cinc-server-ctl upgrade
  grep major /var/opt/opscode/upgrades/migration-level && cinc-server-ctl restart
}

function prevent_dns_overload {
  # erchef will constantly try to DNS lookup the bookshelf hostname, which is simply itself.
  # as a workaround, we're just going to put the hostname
  HOSTENTRY="`curl http://169.254.169.254/latest/meta-data/local-ipv4` `hostname -s`.local"
  grep $HOSTENTRY /etc/hosts || echo $HOSTENTRY >> /etc/hosts
  hostname `hostname -s`.local
}

function bootstrap_first_start () {
	cinc-server-ctl reconfigure
	cinc-server-ctl restart
}

# Fix the hostname before we get to do all the reconfiguration bits
prevent_dns_overload

# If we're not bootstrap - we're a regular node - pull config and start
if [ -z "${BOOTSTRAP_TAGS}" ] ; then
  echo "[INFO] startup.sh : Regular CINC Node Startup"
  wait_for_config
  download_config
  node_start
fi

# If we're a bootstrap and configs already existed in s3, attempt an download, upgrade/start and upload
if [ -n "${BOOTSTRAP_TAGS}" ] && [ -n "${CONFIGS_EXIST}" ] ; then
  echo "[INFO] startup.sh : Bootstrap and Upgrade Startup"
  download_config
  bootstrap_upgrade_and_start
  upload_config
fi

# otherwise we're a bootstrap node and no configs exist in s3 so we bootstrap as if for the first time
if [ -n "${BOOTSTRAP_TAGS}" ] && [ -z "${CONFIGS_EXIST}" ] ; then
  echo "[INFO] startup.sh : Configuring bootstrap cinc-server"
  bootstrap_first_start
  upload_config
fi

echo "[INFO] startup.sh : Finished"
