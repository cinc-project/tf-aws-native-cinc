###########
# Master DB
###########
module "pg_rds_master" {
  count  = var.postgres_backend == "rds" ? 1 : 0
  source = "github.com/terraform-aws-modules/terraform-aws-rds?ref=v2.18.0"

  identifier = "${var.servicename}-${var.environment}-master"

  engine              = "postgres"
  engine_version      = var.pg_engine_version
  instance_class      = var.rds_pg_instance_class
  allocated_storage   = var.rds_pg_allocated_storage
  deletion_protection = var.pg_deletion_protection

  name     = "${var.servicename}${var.environment}"
  username = var.pg_username
  password = local.set_pg_password
  port     = var.pg_port


  vpc_security_group_ids = [aws_security_group.db.id]

  maintenance_window = "Mon:00:00-Mon:03:00"
  backup_window      = "03:00-06:00"

  # Backups are required in order to create a replica
  backup_retention_period = 1

  # DB subnet group
  subnet_ids = [aws_subnet.private_az2.id, aws_subnet.private_az2.id, aws_subnet.private_az3.id]

  create_db_option_group    = false
  create_db_parameter_group = false
  tags = merge(
    var.standard_tags,
    {
      Name = "${var.servicename}-${var.environment}-master"
    },
  )
}

############
# Replica DB
############
module "pg_rds_replica" {
  count  = var.postgres_backend == "rds" ? 1 : 0
  source = "github.com/terraform-aws-modules/terraform-aws-rds?ref=v2.18.0"

  identifier = "${var.servicename}-${var.environment}-replica"

  # Source database. For cross-region use this_db_instance_arn
  replicate_source_db = module.pg_rds_master[0].this_db_instance_id

  engine              = "postgres"
  engine_version      = var.pg_engine_version
  instance_class      = var.rds_pg_instance_class
  allocated_storage   = var.rds_pg_allocated_storage
  deletion_protection = var.pg_deletion_protection

  # Username and password must not be set for replicas
  username = ""
  password = ""
  port     = var.pg_port

  vpc_security_group_ids = [aws_security_group.db.id]

  maintenance_window = "Tue:00:00-Tue:03:00"
  backup_window      = "03:00-06:00"

  # disable backups to create DB faster
  backup_retention_period = 0

  # Not allowed to specify a subnet group for replicas in the same region
  create_db_subnet_group = false

  create_db_option_group    = false
  create_db_parameter_group = false
  tags = merge(
    var.standard_tags,
    {
      Name = "${var.servicename}-${var.environment}-replica"
    },
  )
}
