# we pull the secret password however, from its storage, if we don't have pg_password set, for creating the dbs.
locals {
  set_pg_password = var.pg_password == null ? module.postgres_master_random_password[0].secret : var.pg_password
}

# If variable pg_password is left null we'll securely create a random password and store it in AWS Secret Manager
module "postgres_master_random_password" {
  count       = var.pg_password == null ? 1 : 0
  source      = "github.com/rhythmictech/terraform-aws-secretsmanager-random-secret?ref=v1.2.0"
  name_prefix = "${var.servicename}-${var.environment}-db-password-"
  description = "${var.servicename}-${var.environment}-db-password Auto-generated Postgres Master User Password"
  length      = 64
  use_special = false # postgres is sensitive to most special characters in the password - a long alphanumeric is perfectly fine
}
