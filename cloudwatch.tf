data "aws_iam_policy_document" "cloudwatch" {
  count   = var.cloudwatch_enabled == true ? 1 : 0
  version = "2012-10-17"
  statement {
    actions = [
      "logs:CreateLogGroup",
      "logs:CreateLogStream",
      "logs:PutLogEvents",
      "logs:PutRetentionPolicy",
      "logs:DescribeLogStreams"
    ]
    effect    = "Allow"
    resources = ["arn:aws:logs:*:*:*"]
  }
}


resource "aws_iam_policy" "cloudwatch" {
  count       = var.cloudwatch_enabled == true ? 1 : 0
  name        = "${var.servicename}-${var.environment}-cloudwatch"
  path        = "/"
  description = "Allows CINC services to send logs to CloudWatch"
  policy      = data.aws_iam_policy_document.cloudwatch[0].json
}

resource "aws_iam_role_policy_attachment" "cinc_cloudwatch" {
  count      = var.cloudwatch_enabled == true ? 1 : 0
  role       = aws_iam_role.cinc_cluster.id
  policy_arn = aws_iam_policy.cloudwatch[0].arn
}

# picking path for install style
locals {
  cloudwatch_distro_path_ubuntu = var.cinc_cluster_distro_type == "ubuntu" ? "ubuntu" : ""
  cloudwatch_distro_path_debian = var.cinc_cluster_distro_type == "debian" ? "debian" : ""
  cloudwatch_distro_path_el     = var.cinc_cluster_distro_type == "el" ? "redhat" : ""
  cloudwatch_distro_path_amzn2  = var.cinc_cluster_distro_type == "amzn2" ? "amazon_linux" : ""
  cloudwatch_distro_path_sles   = var.cinc_cluster_distro_type == "sles" ? "suse" : ""
  cloudwatch_distro_path        = coalesce(local.cloudwatch_distro_path_ubuntu, local.cloudwatch_distro_path_debian, local.cloudwatch_distro_path_el, local.cloudwatch_distro_path_amzn2, local.cloudwatch_distro_path_sles)
}

data "template_file" "cloudwatch_userdata" {
  count    = var.cloudwatch_enabled == true ? 1 : 0
  template = file("${path.module}/cloudwatch_userdata.txt")

  vars = {
    var_servicename              = var.servicename
    var_environment              = var.environment
    var_aws_region               = var.aws_region
    var_cloudwatch_log_retention = var.cloudwatch_log_retention
    local_cloudwatch_distro_path = local.cloudwatch_distro_path
    local_package_type           = local.package_type
    local_packager_cmd           = local.packager_cmd
  }
}
