resource "aws_key_pair" "accesskey" {
  key_name   = "${var.servicename}-${var.environment}"
  public_key = var.ec2_pubkey
}

# Security group for the ELB so it is accessible via the web
resource "aws_security_group" "elb" {
  name        = "${var.servicename}-${var.environment}-elb"
  vpc_id      = aws_vpc.default.id
  description = "Security group for the ELB so it is accessible from anywhere"

  # HTTPS access from anywhere
  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = var.clientranges
  }

  # HTTP access from anywhere (to redirect to https at ELB level)
  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = var.clientranges
  }

  # Outbound internet access
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = [var.anywhere]
  }

  tags = merge(
    var.standard_tags,
    {
      Name = "${var.servicename}-${var.environment}-elb"
    },
  )
}



# chefserver security group
resource "aws_security_group" "cinc_cluster" {
  name   = "${var.servicename}-${var.environment}-cluster"
  vpc_id = aws_vpc.default.id

  # SSH access from trusted ips
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "TCP"
    cidr_blocks = var.adminranges
  }

  # HTTPS access from VPC (loadbalancers)
  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "TCP"
    cidr_blocks = [var.cidr]
  }

  # outbound internet access
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = [var.anywhere]
  }

  tags = merge(
    var.standard_tags,
    {
      Name = "${var.servicename}-${var.environment}-cluster"
    },
  )
}

# db security group
resource "aws_security_group" "db" {
  name   = "${var.servicename}-${var.environment}-db"
  vpc_id = aws_vpc.default.id

  # Postgres access from vpc cidr
  ingress {
    from_port   = var.pg_port
    to_port     = var.pg_port
    protocol    = "TCP"
    cidr_blocks = [var.cidr]
  }

  # Elasticsearch access from cidr
  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "TCP"
    cidr_blocks = [var.cidr]
  }

  tags = merge(
    var.standard_tags,
    {
      Name = "${var.servicename}-${var.environment}-db"
    },
  )
}
