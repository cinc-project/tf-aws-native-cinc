resource "aws_iam_instance_profile" "cinc_cluster" {
  name = "${var.servicename}-${var.environment}-cluster"
  role = aws_iam_role.cinc_cluster.name
}

data "aws_iam_policy_document" "assume_role" {
  version = "2012-10-17"
  statement {
    actions = ["sts:AssumeRole"]
    effect  = "Allow"
    principals {
      type        = "Service"
      identifiers = ["ec2.amazonaws.com"]
    }
  }
}


resource "aws_iam_role" "cinc_cluster" {
  name               = "${var.servicename}-${var.environment}-cluster"
  path               = "/"
  assume_role_policy = data.aws_iam_policy_document.assume_role.json
}

data "aws_iam_policy_document" "cinc_cluster" {
  version = "2012-10-17"
  statement {
    actions = [
      "ec2:Describe*",
      "ec2:CreateSnapshot",
      "ec2:CreateTags",
      "ec2:DeleteSnapshot",
      "ec2:DescribeSnapshots",
      "ec2:DescribeVolumes",
      "s3:List*",
      "autoscaling:*",
      "cloudwatch:PutMetricData",
      "cloudwatch:GetMetricStatistics",
      "cloudwatch:ListMetrics"
    ]
    effect    = "Allow"
    resources = ["*"]
  }

  statement {
    actions = [
      "logs:DescribeLogStreams",
      "logs:PutLogEvents",
      "logs:CreateLogStream",
      "logs:CreateLogGroup"
    ]
    effect    = "Allow"
    resources = ["arn:aws:logs:${var.aws_region}:*:log-group:*"]
  }

  statement {
    actions = [
      "secretsmanager:GetSecretValue"
    ]
    effect    = "Allow"
    resources = ["arn:aws:secretsmanager:${var.aws_region}:*:secret:${var.servicename}-${var.environment}-db-password-*"]
  }

}

resource "aws_iam_policy" "cinc_cluster" {
  name        = "${var.servicename}-${var.environment}-cluster-policy"
  description = "permissions to do logging, clustering, s3 listing etc"
  policy      = data.aws_iam_policy_document.cinc_cluster.json
}

resource "aws_iam_role_policy_attachment" "cinc_nodes" {
  role       = aws_iam_role.cinc_cluster.name
  policy_arn = aws_iam_policy.cinc_cluster.arn
}
