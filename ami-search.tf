# finding the latest ami for different kinds of distro

data "aws_ami" "ubuntu" {
  count       = var.cinc_cluster_distro_type == "ubuntu" ? 1 : 0
  most_recent = true
  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-*-${var.cinc_cluster_distro_version}-amd64-server-*"]
  }
  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
  owners = ["099720109477"] # Canonical
}

data "aws_ami" "debian" {
  count       = var.cinc_cluster_distro_type == "debian" ? 1 : 0
  most_recent = true
  filter {
    name   = "name"
    values = ["debian-${var.cinc_cluster_distro_version}-amd64-*"]
  }
  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
  owners = ["136693071363"] # Debian Project
}

data "aws_ami" "el" {
  count       = var.cinc_cluster_distro_type == "el" ? 1 : 0
  most_recent = true
  filter {
    name   = "name"
    values = ["RHEL-${var.cinc_cluster_distro_version}.*_HVM_GA-*-x86_64*"]
  }
  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
  owners = ["309956199498"] # Redhat
}

data "aws_ami" "amzn2" {
  count       = var.cinc_cluster_distro_type == "amzn2" ? 1 : 0
  most_recent = true
  filter {
    name   = "name"
    values = ["amzn2-ami-hvm-2.0.*-x86_64-ebs"]
  }
  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
  owners = ["amazon"]
}

data "aws_ami" "sles" {
  count       = var.cinc_cluster_distro_type == "sles" ? 1 : 0
  most_recent = true
  filter {
    name   = "name"
    values = ["suse-sles-${var.cinc_cluster_distro_version}*-hvm-ssd-x86_64"]
  }
  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
  owners = ["amazon"]
}
