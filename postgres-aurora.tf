###########
# Aurora PG DB - line-compatiable PG
###########
module "pg_aurora" {
  count  = var.postgres_backend == "aurora" ? 1 : 0
  source = "github.com/terraform-aws-modules/terraform-aws-rds-aurora?ref=v2.28.0"

  name = "${var.servicename}-${var.environment}-pg"

  engine         = "aurora-postgresql"
  engine_version = var.pg_engine_version

  vpc_id  = aws_vpc.default.id
  subnets = [aws_subnet.private_az2.id, aws_subnet.private_az2.id, aws_subnet.private_az3.id]

  replica_scale_enabled   = var.aurora_pg_replica_scale_enabled
  replica_scale_min       = var.aurora_pg_replica_scale_min
  replica_scale_max       = var.aurora_pg_replica_scale_max
  replica_count           = var.aurora_pg_replica_count
  allowed_security_groups = [aws_security_group.cinc_cluster.id]
  allowed_cidr_blocks     = [var.cidr]
  instance_type           = var.aurora_pg_instance_type
  storage_encrypted       = var.aurora_pg_storage_encrypted
  apply_immediately       = var.aurora_pg_apply_immediately
  monitoring_interval     = var.aurora_pg_monitoring_interval
  deletion_protection     = var.pg_deletion_protection

  username = var.pg_username
  password = local.set_pg_password
  port     = var.pg_port

  db_parameter_group_name         = aws_db_parameter_group.aurora_db_postgres9_parameter_group[0].id
  db_cluster_parameter_group_name = aws_rds_cluster_parameter_group.aurora_cluster_postgres9_parameter_group[0].id

  #enabled_cloudwatch_logs_exports = ["audit", "error", "general", "slowquery"] not currently working

  tags = merge(
    var.standard_tags,
    {
      Name = "${var.servicename}-${var.environment}-pg"
    },
  )
}

resource "aws_db_parameter_group" "aurora_db_postgres9_parameter_group" {
  count       = var.postgres_backend == "aurora" ? 1 : 0
  name        = "${var.servicename}-${var.environment}-postgres9-db-parameter-group"
  family      = "aurora-postgresql9.6"
  description = "${var.servicename}-${var.environment}-postgres9-db-parameter-group"
}

resource "aws_rds_cluster_parameter_group" "aurora_cluster_postgres9_parameter_group" {
  count       = var.postgres_backend == "aurora" ? 1 : 0
  name        = "${var.servicename}-${var.environment}-postgres9-cluster-parameter-group"
  family      = "aurora-postgresql9.6"
  description = "${var.servicename}-${var.environment}-postgres9-cluster-parameter-group"
}
