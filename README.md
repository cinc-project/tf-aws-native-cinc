## Terraform code to setup an AWS-native CINC Server

# EARLY RELEASE CODE WARNING LABEL

This is new code (as of) October 2020 and under Active Development. As of writing even CINC Server is not considered 'stable' and has only received limited testing. This is code to deploy CINC Server so even if this is well written, we're dealing with not one but *two* relative unknowns.

Things will change on this repo that might break your infra if you use it in its current state and then try and use a later version. For example we want to make certain parts of the dependencies modular. We'll try to build this so those features, when added, will be more stable to use.

If this happens we recommend getting used to the terraform state mv command - it's a really useful function.

If you're not confident, don't use this repo until we start tagging releases and publishing it to the terraform module registry.

However even at this stage we consider this a cleaner thing to look at than the ANCS cloudformation code that this was inspired by.

But regardless of the above we encourage you to try and use this for your needs and provide feedback. Better yet, submit patches via the merge-request function.

# OBJECTIVES

TL/DR: This is a terraform codebase to run an AWS NATIVE deployment of the CINC server from https://cinc.sh/

The initial version is designed as a like-for-like of the https://github.com/chef-customers/aws_native_chef_server/ aka ANCS repo.

This repo aims to achieve the similar goals to those aimed at by the ANCS repo :
- A modular scaling group setup of CHEF server (in our case CINC build).
- AWS Service for postgres
- AWS Service for elasticsearch

However we recognise things have moved on since the ANCS design was implemented, as such.. the native CINC deploy will be modernized.

Key differences :
- This is implemented in terraform, not cloudformation.
- There are no 'built' amis involved - fewer dependant steps, no packer.
- By default we just use ubuntu images directly and drop a deb of the cinc-core server on it. We can use other images too.
- ANCS's main.sh has been renamed startup.sh for clarity/sanity.
- This currently only implements the CINC server, no bells and whistles management interface or push-jobs or supermarket or automate.
- Other kinds of Postgres backend.

Terraform considerations :
- This repo aims to be somewhere between a 'module' and a 'top level repo'.
- Some components that start out in here might become repos of their own, or sub-modules.

Fork it, duplicate it, and start making your own AWS-native cinc infra !

# HOW TO USE

Fork or Duplicate this repo and use it directly.

Or call it as a git-sourced terraform module repo with a tag (I'd suggest forking it just to stay safe)

See INPUTS OVERVIEW, TUNING POTENTIAL / EXAMPLES, INPUTS and OUTPUTS sections of this document.

```
$ terraform init
$ terraform plan
$ terraform apply
```

ssh onto the bootstrap node as ubuntu user and then create your cinc users and org (and so on) - see the `bootstrap_ssh_line` output for a helpful hint.

```
ssh ubuntu@hostip -i ~/.ssh/bob_cinc_admin_rsa
...
sudo cinc-server-ctl user-create bob Bob Salarybob bob@bobcorp.net REALLYEASILYGUESSEDPASSWORD --filename ~/bob.pem
sudo cinc-server-ctl org-create bobcorp "BobCorp" --association_user bob --filename ~/bobcorp-validator.pem
```

it's chef/cinc all the way down from here. see the `api_hostname` output for clients reference the chef docs!

# CONCEPTS TO UNDERSTAND

There are two scaling groups used to run this service.

1. the cinc-bootscrap scaling group - a cinc core server of 1 to initialize databases and provide a minimal service
2. the cinc-nodes scaling group - any number of cinc core servers you need to handle your load in addition to the bootstrap

Both of these scaling groups run identical user-data but the startup.sh at the end of the user-data will do different things based on the asg cluster member name

On the bootstrap cluster member, if no config exists in s3, we generate the initial secrets and publish them.
The cinc-nodes cluster members will wait for the initialised config (keys, etc) to be published to s3.

See `startup.sh` for closer examination of the process.

when applying the infra the cinc-nodes cluster depends on the cinc-bootstrap cluster :

```
aws_autoscaling_group.cinc-bootstrap: Still creating... [40s elapsed]
....
aws_autoscaling_group.cinc-bootstrap: Still creating... [1m10s elapsed]
aws_autoscaling_group.cinc-bootstrap: Creation complete after 1m19s [id=bootstrap-cinc-poc-XX]
aws_autoscaling_group.cinc-nodes: Creating...
aws_autoscaling_group.cinc-nodes: Still creating... [10s elapsed]
....
aws_autoscaling_group.cinc-nodes: Still creating... [50s elapsed]
aws_autoscaling_group.cinc-nodes: Creation complete after 53s [id=nodes-cinc-poc-XX]
```

The above described behaviour effectively replace the ANCS's main.sh cloudformation readyness callbacks.

# TUNING POTENTIAL / EXAMPLES

Ultimately, different users have different requirements. We hope this single module can satisfy most requirements with the right combination of variables.

- ultra-low-budget proof-of-concept testing ?

Look at at the files and settings in the `example/small-cinc-infra/` example. This is the smallest infra we consider viable for proving things out.

- mid-budget development infra ? 100-1000 client nodes?

Look at a `t2.large` machine and consider either the rds/aurora options based on your budget.

- big-budget high-scalability prod infra ? 10K+ client nodes ?

Consider a `t2.xlarge` or greater machine size, watch the load and tune `cinc_nodes_max_size` accordingly, make sure to look at the aurora options for high performant aurora postgres like `aurora_pg_replica_scale_max` and consider how your elasticsearch cluster is configured based on your search load.

We'd recommend you review your load and budget at a timely interval. Here are some urls related to options.

https://aws.amazon.com/elasticsearch-service/pricing/
https://docs.aws.amazon.com/elasticsearch-service/latest/developerguide/aes-limits.html
https://aws.amazon.com/rds/aurora/pricing/
https://docs.aws.amazon.com/AmazonRDS/latest/AuroraUserGuide/Concepts.DBInstanceClass.html#Concepts.DBInstanceClass.SupportAurora

If some variable or functionality is missing that needs tuning, please submit issues or (better yet) patches.

# MODULES

This codebase makes use of the following excellent modules:

- github.com/terraform-community-modules/tf_aws_elasticsearch [for elasticsearch]
- github.com/terraform-aws-modules/terraform-aws-rds-aurora [for aurora style posgres - high cost high scalability option]
- github.com/terraform-aws-modules/terraform-aws-rds [for rds style postgres - lower cost dev env option]
- github.com/rhythmictech/terraform-aws-secretsmanager-random-secret [for a random password for the postgres master user]

The "aurora" or "rds" options can be set via the `postgres_backend` variable.

# CONTACT, FURTHER READING, ETC

We're on the Chef Community Slack, specifically the #community-distros channel. See https://community-slack.chef.io/ for signup.

CINC is a Free-as-in-Beer distribution of the open source software of Chef Software Inc.

* https://cinc.sh/
* https://chef.io/
* https://docs.chef.io/
* https://community.chef.io/

# INPUTS OVERVIEW

Many inputs are needed to make this work appropriately, and while they are auto-documented in the next section, here we go into a little more detail about the core vars and their interrelationships:

Top-Level Variables: (non-optional to get a working deployment):
- `cert_arn` - ACM certificate ARN
- `dns_zone` - DNS Zone - this must work together with the `cert_arn` and `servicename` which will be created in the zone.
- `aws_region` -  pick your desired region. The cert for which `cert_arn` is set MUST be in the same `aws_region`.
- `ec2_pubkey` - Required Administration key for SSH access - private part needed to ssh in and generate users and orgs see `adminkeys` to supply additional ones.
- `environment` - is this for `dev` or `prod` or `bobdev` ? supply what it is. Combines with `servicename` for naming cloud objects.
- `servicename` - defaults to `cinc` - also the reused as the name of the dns entry that goes in the `dns_zone`. Combines with `environment` for naming cloud objects.

We recommend aligning `environment` with a subdomain if that is what you will set for `dns_zone`.

Crucial Network Security Variables
- `clientranges` - `["192.168.0.0/24"]` by default, can be used to lock down (or open up) incoming clients for api access.
- `adminranges` - `["192.168.0.0/24"]` by default, can be used to lock down (or open up) incoming ssh administration.

Database Variables:
- `postgres_backend` - `rds` by default, can be set to `aurora` for improved reliability and scaling and associated higher costs.
- `pg_password` - by default, we'll generate a random one (stored in AWS secrets manager) if this is left null. Otherwise, the value set is used.

More variables are shown in `example/cinc-infra.tf` (a modular example) and see the following auto-documented info generated from `variables.tf`.

<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Requirements

| Name | Version |
|------|---------|
| terraform | >= 0.13 |
| aws | ~> 3.9 |
| random | ~> 2.3.0 |
| template | ~> 2.2.0 |

## Providers

| Name | Version |
|------|---------|
| aws | ~> 3.9 |
| template | ~> 2.2.0 |

## Inputs

| Name | Description | Type | Default |
|------|-------------|------|---------|
| adminkeys | additional keypairs | `string` | `""` |
| adminranges | admin ranges for ssh access etc - make this as open as you need it to be | `list` | <pre>[<br>  "192.168.0.0/24"<br>]</pre> |
| anywhere | CIDR range of anywhere | `string` | `"0.0.0.0/0"` |
| aurora\_pg\_apply\_immediately | Determines whether or not any DB modifications are applied immediately, or during the maintenance window | `bool` | `false` |
| aurora\_pg\_instance\_type | Instance type to use at master instance. If instance\_type\_replica is not set it will use the same type for replica instances | `string` | `"db.r4.large"` |
| aurora\_pg\_monitoring\_interval | The interval (seconds) between points when Enhanced Monitoring metrics are collected | `number` | `10` |
| aurora\_pg\_replica\_count | Number of reader nodes to create. If aurora\_pg\_replica\_scale\_enable is true, the value of aurora\_pg\_replica\_scale\_min is used instead. | `number` | `1` |
| aurora\_pg\_replica\_scale\_enabled | Enable to allow the aurora nodes to scale (with cpu load) | `bool` | `false` |
| aurora\_pg\_replica\_scale\_max | Maximum number of reader nodes to create when replica\_scale\_enable is enabled | `number` | `5` |
| aurora\_pg\_replica\_scale\_min | Minimum number of reader nodes to create when replica\_scale\_enable is enabled | `number` | `1` |
| aurora\_pg\_storage\_encrypted | Specifies whether the underlying storage layer should be encrypted | `bool` | `true` |
| autoscaling\_down\_threshold | sustained load under which we decrease the instance count | `number` | `40` |
| autoscaling\_enabled | can be set to `false`, used to disable autoscaling if you don't want it. Recommend you leave it enabled! | `bool` | `true` |
| autoscaling\_up\_threshold | sustained load over which we increase the instance count | `number` | `60` |
| aws\_region | the AWS region to operate in | `string` | n/a |
| cert\_arn | AWS ARN to the certificate to be associated to the ALB to enable HTTPS connections | `string` | n/a |
| cidr | CDIR range to be used for the VPC of the cinc deploy and dependant services (Postgres, ES) | `string` | `"172.26.198.0/24"` |
| cinc\_build\_flavour | cinc server builds are currently under the unstable branch but will be stable sooner or later after 2010/10 | `string` | `"unstable"` |
| cinc\_build\_path | cinc offers several build packages targeting | `string` | `"ubuntu/18.04"` |
| cinc\_cluster\_distro\_ami | this can be used to over-ride the distro search with a specific AMI. | `string` | `null` |
| cinc\_cluster\_distro\_type | the distro name for finding the AMI - see also cinc\_cluster\_distro\_version | `string` | `"ubuntu"` |
| cinc\_cluster\_distro\_version | the version of the distro chosen with the cinc\_cluster\_distro\_type var | `string` | `"18.04"` |
| cinc\_mirror\_base\_path | base url to find packages | `string` | `"http://downloads.cinc.sh/files/"` |
| cinc\_nodes\_desired\_capacity | initial desired capacity for the node cluster scaling group - this will be ignored once the load is tracked | `number` | `1` |
| cinc\_nodes\_max\_size | maximum number of nodes the node cluster can scale to | `number` | `10` |
| cinc\_nodes\_min\_size | minimum number of nodes the node cluster can scale to | `number` | `1` |
| cinc\_server\_version | cinc will provide more builds from 2020/10 | `string` | `"14.0.58"` |
| clientranges | client ranges for cinc/chef clients - make this as open as you need it to be | `list` | <pre>[<br>  "192.168.0.0/24"<br>]</pre> |
| cloudwatch\_enabled | can be set to false if you need alternative logging - you can inject your own via custom\_userdata variables. | `bool` | `true` |
| cloudwatch\_log\_retention | how long to retain logs in cloudwatch for | `number` | `90` |
| core\_server\_installer\_url | optional variable to set the path to the url directly i.e. a local bucket or specific mirror | `string` | `""` |
| custom\_userdata\_end | additional userdata at the end of the run - a nice place to inject customizations | `string` | `"#!/bin/bash -v"` |
| custom\_userdata\_start | additional userdata at the start of the run - a nice place to inject customizations | `string` | `"#!/bin/bash -v"` |
| defaultorgname | a default org name if one is desired | `string` | `""` |
| dns\_zone | the zone in which we'll create the api end point using servicename var | `string` | n/a |
| ec2\_instance\_type | machine type for the boostrap and node clusters - we suggest t2.medium minimum | `string` | `"t2.medium"` |
| ec2\_pubkey | ssh keypair that must be supplied | `string` | n/a |
| environment | the environment name of the deploy - something like 'prod' or 'dev' or 'testing' - consider your requirements | `string` | n/a |
| es\_create\_iam\_service\_linked\_role | Whether to create IAM service linked role for AWS ElasticSearch service. Can be only once per AWS account. KLUDGE. | `bool` | `true` |
| es\_dedicated\_master\_type | the elasticsearch dedicated master instance type | `string` | `"t2.small.elasticsearch"` |
| es\_ebs\_volume\_size | the EBS volume size for the elasticsearch cluster - this aligns with the note times for the elasticsearch cluster | `number` | `35` |
| es\_instance\_count | how many elasticsearch instances to create - aka shard count | `number` | `3` |
| es\_instance\_type | the elasticsearch instance type | `string` | `"t2.small.elasticsearch"` |
| es\_replica\_count | how many elasticsearch replicas of data there will be stored when writing | `number` | `2` |
| es\_version | Version of Elasticsearch | `number` | `6.8` |
| es\_zone\_awareness | Enable zone awareness for Elasticsearch cluster | `bool` | `true` |
| es\_zone\_awareness\_count | needs to be multiple of the instance\_count (x1, x2, x3) if we set es\_zone\_awareness | `number` | `3` |
| pg\_deletion\_protection | Set to false if you want to allow terraform to delete your postgres database. Recommend doing that by hand if you have to. | `bool` | `true` |
| pg\_engine\_version | version of posgres - see the AWS RDS or AWS Aurora specifications to get the available versions | `string` | `"9.6.12"` |
| pg\_password | We'll generate a random one (stored in AWS secrets manager) if this is left null. Otherwise, the value set is used. | `string` | `null` |
| pg\_port | the postgress port to use | `string` | `"5432"` |
| pg\_username | master postgres username for the cinc server to use | `string` | `"cincadmin"` |
| postgres\_backend | rds or aurora - rds is cheaper, aurora is more available and scales better | `string` | `"rds"` |
| pushjobs\_enabled | can be set to `true`, if you want to enable push-jobs networking. In its current state this will require your own code in one of the `custom_userdata_*` variables. Pushjobs will only run on the bootstrap node. | `bool` | `false` |
| rds\_pg\_allocated\_storage | RDS-only - allocated storage in gigabytes | `number` | `5` |
| rds\_pg\_instance\_class | The instance type of the RDS instance | `string` | `"db.t3.small"` |
| servicename | the service name | `string` | `"cinc"` |
| ssh\_user | optional variable to set ssh user name | `string` | `null` |
| standard\_tags | Standard resource tag used for all resources that can take them | `map(string)` | <pre>{<br>  "Managedby": "terraform",<br>  "Module": "aws-native-cinc"<br>}</pre> |
| update\_userdata | optional variable to set the update commands - this might over-ride a choice we make | `string` | `null` |

## Outputs

| Name | Description |
|------|-------------|
| api\_hostname | point your API clients (cinc nodes, knife config) at this hostname |
| bootstrap\_ssh\_line | a hint at how to log onto the bootstrap node, in order to create orgs and users - be sure to copy the keys off after they are created, nodes aren't immutable |

<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->

# CLUSTER OS CHOICE OPTIONS

The following mixes have been tested with `cinc_server_version` = `"14.0.58"`

We've even created the deploy with one distro and then switched to another - Not that this is USEFUL but it proves that the processing layer represented by the bootstrap and node clusters are properly separated from the database layers.

The following are tested and working without customization :

- ubuntu (module default - most tested) :

```
cinc_cluster_distro_type = "ubuntu"
cinc_cluster_distro_version = "18.04"
cinc_build_path = "ubuntu/18.04"
```

- amazon linux 2 :

```
cinc_cluster_distro_type = "amzn2"
cinc_build_path = "el/7"
```

- debian 10 :

```
cinc_cluster_distro_type = "debian"
cinc_cluster_distro_version = "10"
cinc_build_path = "debian/10"
```

- sles (suse) 15 :

```
cinc_cluster_distro_type = "sles"
cinc_cluster_distro_version = "15"
cinc_build_path = "sles/15"
```

The following is supported with some additional customization:

- redhat 7 :

```
cinc_cluster_distro_type = "el"
cinc_cluster_distro_version = "7"
cinc_build_path = "el/7"
# (redhat 7 is quite old needs the following custom_userdata_start var content )
custom_userdata_start = <<EOT
#!/bin/bash -v
yum install -y python3-pip
pip3 install awscli
ln -s /usr/local/bin/aws /usr/bin/aws
EOT
```
