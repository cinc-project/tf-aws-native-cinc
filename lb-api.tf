resource "aws_lb" "cinc" {
  name               = "${var.servicename}-${var.environment}"
  internal           = false
  load_balancer_type = "application"
  security_groups    = [aws_security_group.elb.id]
  subnets            = [aws_subnet.public_az1.id, aws_subnet.public_az2.id, aws_subnet.public_az3.id]
  tags = merge(
    var.standard_tags,
    {
      Name = "${var.servicename}-${var.environment}"
    },
  )
}

# Look up the Route53 Hosted Zone by domain name
data "aws_route53_zone" "selected" {
  name = "${var.dns_zone}."
}

resource "aws_route53_record" "cinc" {
  zone_id = join("", data.aws_route53_zone.selected.*.zone_id)
  name    = var.servicename
  type    = "CNAME"
  ttl     = 300
  records = [aws_lb.cinc.dns_name]
}

resource "aws_lb_listener" "cinc" {
  load_balancer_arn = aws_lb.cinc.arn
  port              = 443
  protocol          = "HTTPS"
  ssl_policy        = "ELBSecurityPolicy-TLS-1-2-2017-01"
  certificate_arn   = var.cert_arn

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.cinc_nodes.arn
  }
}

resource "aws_lb_listener" "httpredirect" {
  load_balancer_arn = aws_lb.cinc.arn
  port              = "80"
  protocol          = "HTTP"

  default_action {
    type = "redirect"

    redirect {
      port        = "443"
      protocol    = "HTTPS"
      status_code = "HTTP_301"
    }
  }
}

resource "aws_lb_target_group" "cinc_bootstrap" {
  name        = "${var.servicename}-${var.environment}-bootstrap"
  port        = 443
  protocol    = "HTTPS"
  vpc_id      = aws_vpc.default.id
  target_type = "instance"
  health_check {
    healthy_threshold   = 2
    unhealthy_threshold = 2
    interval            = 10
    path                = "/_status"
    protocol            = "HTTPS"
  }
  tags = merge(
    var.standard_tags,
    {
      Name = "${var.servicename}-${var.environment}-bootstrap"
    },
  )
}

resource "aws_lb_target_group" "cinc_nodes" {
  name        = "${var.servicename}-${var.environment}-nodes"
  port        = 443
  protocol    = "HTTPS"
  vpc_id      = aws_vpc.default.id
  target_type = "instance"
  health_check {
    healthy_threshold   = 2
    unhealthy_threshold = 2
    interval            = 10
    path                = "/_status"
    protocol            = "HTTPS"
  }
  tags = merge(
    var.standard_tags,
    {
      Name = "${var.servicename}-${var.environment}-nodes"
    },
  )
}
