# cpu load based
# scale up alarm
resource "aws_autoscaling_policy" "cpu_policy_scaleup" {
  count                  = var.autoscaling_enabled == true ? 1 : 0
  name                   = "${var.servicename}-${var.environment}-cpu-load-scaleup"
  autoscaling_group_name = aws_autoscaling_group.cinc_nodes.name
  adjustment_type        = "ChangeInCapacity"
  scaling_adjustment     = "1"
  cooldown               = "180"
  policy_type            = "SimpleScaling"
}

# scale down alarm
resource "aws_autoscaling_policy" "cpu_policy_scaledown" {
  count                  = var.autoscaling_enabled == true ? 1 : 0
  name                   = "${var.servicename}-${var.environment}-cpu-load-scaledown"
  autoscaling_group_name = aws_autoscaling_group.cinc_nodes.name
  adjustment_type        = "ChangeInCapacity"
  scaling_adjustment     = "-1"
  cooldown               = "360"
  policy_type            = "SimpleScaling"
}

resource "aws_cloudwatch_metric_alarm" "cpu_alarm_scaleup" {
  count               = var.autoscaling_enabled == true ? 1 : 0
  alarm_name          = "${var.servicename}-${var.environment}-cpu_alarm_scaleup"
  alarm_description   = "${var.servicename}-${var.environment}-cpu_alarm_scaleup"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "5"
  metric_name         = "CPUUtilization"
  namespace           = "AWS/EC2"
  period              = "60"
  statistic           = "Average"
  threshold           = var.autoscaling_up_threshold
  dimensions = {
    "AutoScalingGroupName" = aws_autoscaling_group.cinc_nodes.name
  }
  actions_enabled = true
  alarm_actions   = [aws_autoscaling_policy.cpu_policy_scaleup[0].arn]
}


resource "aws_cloudwatch_metric_alarm" "cpu_alarm_scaledown" {
  count               = var.autoscaling_enabled == true ? 1 : 0
  alarm_name          = "${var.servicename}-${var.environment}-cpu_alarm_scaledown"
  alarm_description   = "${var.servicename}-${var.environment}-cpu_alarm_scaledown"
  comparison_operator = "LessThanOrEqualToThreshold"
  evaluation_periods  = "10"
  metric_name         = "CPUUtilization"
  namespace           = "AWS/EC2"
  period              = "60"
  statistic           = "Average"
  threshold           = var.autoscaling_down_threshold
  dimensions = {
    "AutoScalingGroupName" = aws_autoscaling_group.cinc_nodes.name
  }
  actions_enabled = true
  alarm_actions   = [aws_autoscaling_policy.cpu_policy_scaledown[0].arn]
}
