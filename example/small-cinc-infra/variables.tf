variable "standard_tags" {
  default = {
    "Owner" : "CINC Admins",
    "Billing" : "EXPENSE-ACCOUNT",
    "Service" : "cinc",
    "Environment" : "poc",
  }
  description = "Standard resource tag used for all resources that can take them"
  type        = map(string)
}
