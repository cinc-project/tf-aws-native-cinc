terraform {
  backend "s3" {
    bucket = "CHANGEME-BUCKET-NAME"
    key    = "tf-aws-native-cinc-poc-state.tfstate"
    region = "eu-west-1"
  }
}

provider aws {
  region = var.aws_region
}

terraform {
  required_version = ">= 0.13"
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.9"
    }
    template = {
      source  = "hashicorp/template"
      version = "~> 2.2.0"
    }
    random = {
      source  = "hashicorp/random"
      version = "~> 2.3.0"
    }
  }
}
