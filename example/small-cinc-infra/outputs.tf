output "bootstrap_ssh_line" {
  value       = module.aws_native_cinc.bootstrap_ssh_line
  description = "hint at how to ssh to the bootstrap node to create org and users on"
}
