module "aws_native_cinc" {
  source                            = "git::https://gitlab.com/cinc-project/tf-aws-native-cinc.git?ref=0.1.2" # we suggest tagging a release with ?ref=tagname
  aws_region                        = "eu-west-1"                                                             # pick your region - it should be local to your cinc-clients.
  servicename                       = "cinc"                                                                  # two vars often used together to identify the purpose of the infrastructure
  environment                       = "poc"                                                                   # two vars often used together to identify the purpose of the infrastructure
  standard_tags                     = var.standard_tags                                                       # see above
  ec2_instance_type                 = "t2.medium"                                                             # probably the smallest recommendable size based on memory constraints alone
  cinc_nodes_max_size               = 5                                                                       # could be lower but this just represents a potential ceiling, not what the infrastructure will settle at.
  cinc_nodes_desired_capacity       = 1                                                                       # initial desired capacity - this will be ignored once the load is tracked
  rds_pg_instance_class             = "db.t3.small"                                                           # this won't be expensive, but won't perform at scale either
  rds_pg_allocated_storage          = 5                                                                       # should be fine for a low-scale infra, but, consider more..
  es_instance_type                  = "t2.small.elasticsearch"                                                # the spec of the es instance
  es_ebs_volume_size                = 35                                                                      # matches the pg_instance_type - there are limits that vary based on the instance chosen
  es_instance_count                 = 3                                                                       # a sensible minimum
  es_zone_awareness_count           = 3                                                                       # should be a multiple of the instance count
  es_replica_count                  = 1                                                                       # just one copy this is a poc, increase to two for genuine redundancy
  es_create_iam_service_linked_role = true                                                                    # set to false if already done by another process
  dns_zone                          = "poc.domain.com"                                                        # this domain must exist in route53 - this example subdomain matches the environment setting for good orthogonality.
  cidr                              = "10.0.0.0/8"                                                            # this var used to set the cidr of the vpc that will be created by the terraform module.
  cert_arn                          = "arn:aws:acm:eu-west-1:ACCOUNTID:certificate/CERTID"                    # this cert must exist in the aws_region
  ec2_pubkey                        = "SETME"                                                                 # the primary ssh ec2 key for accessing / creating the ec2 instances
  adminkeys                         = "SETME"                                                                 # additional admins ssh keys
  adminranges                       = ["10.0.0.0/8"]                                                          # use this var to set your admin source ip for ssh to the bootstrap, suggest a series of trusted admin ips/ranges, perhaps your org already has a list in the form of a var in another module ?
  clientranges                      = ["10.0.0.0/8", "172.16.0.0/12", "192.168.0.0/16"]                       # rfc1918 example, make this as wide as you need it to be
}
