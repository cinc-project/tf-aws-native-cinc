# ---------------------------------------------------------------------------------------------------------------------
# REQUIRED PARAMETERS
# You must provide a value for each of these parameters.
# ---------------------------------------------------------------------------------------------------------------------

variable "core_server_installer_url" {
  default     = ""
  description = "optional variable to set the path to the url directly i.e. a local bucket or specific mirror"
  type        = string
}

variable "update_userdata" {
  default     = null
  description = "optional variable to set the update commands - this might over-ride a choice we make"
  type        = string
}

variable "ssh_user" {
  default     = null
  description = "optional variable to set ssh user name"
  type        = string
}

variable "cinc_cluster_distro_ami" {
  default     = null
  description = "this can be used to over-ride the distro search with a specific AMI."
  type        = string
}

variable "cinc_mirror_base_path" {
  default     = "http://downloads.cinc.sh/files/"
  description = "base url to find packages"
  type        = string
}

# software choice
variable "cinc_build_flavour" {
  default     = "unstable"
  description = "cinc server builds are currently under the unstable branch but will be stable sooner or later after 2010/10"
  type        = string
}

variable "cinc_build_path" {
  default     = "ubuntu/18.04"
  description = "cinc offers several build packages targeting"
  type        = string
}

variable "cinc_server_version" {
  default     = "14.0.58"
  description = "cinc will provide more builds from 2020/10"
  type        = string
}

# machine launch
variable "cinc_cluster_distro_type" {
  default     = "ubuntu"
  description = "the distro name for finding the AMI - see also cinc_cluster_distro_version"
  type        = string
}

variable "cinc_cluster_distro_version" {
  default     = "18.04"
  description = "the version of the distro chosen with the cinc_cluster_distro_type var"
  type        = string
}

variable "aws_region" {
  description = "the AWS region to operate in"
  type        = string
}

variable "servicename" {
  default     = "cinc"
  description = "the service name"
  type        = string
}

variable "environment" {
  description = "the environment name of the deploy - something like 'prod' or 'dev' or 'testing' - consider your requirements"
  type        = string
}

variable "standard_tags" {
  default = {
    "Managedby" : "terraform",
    "Module" : "aws-native-cinc",
  }
  description = "Standard resource tag used for all resources that can take them"
  type        = map(string)
}

variable "ec2_instance_type" {
  default     = "t2.medium"
  description = "machine type for the boostrap and node clusters - we suggest t2.medium minimum"
  type        = string
}

variable "cinc_nodes_max_size" {
  default     = 10
  description = "maximum number of nodes the node cluster can scale to"
  type        = number
}

variable "cinc_nodes_min_size" {
  default     = 1
  description = "minimum number of nodes the node cluster can scale to"
  type        = number
}

variable "cinc_nodes_desired_capacity" {
  default     = 1
  description = "initial desired capacity for the node cluster scaling group - this will be ignored once the load is tracked"
  type        = number
}

variable "pg_deletion_protection" {
  default     = true
  description = "Set to false if you want to allow terraform to delete your postgres database. Recommend doing that by hand if you have to."
  type        = bool
}

variable "pg_engine_version" {
  default     = "9.6.12"
  description = "version of posgres - see the AWS RDS or AWS Aurora specifications to get the available versions"
  type        = string
}

variable "postgres_backend" {
  default     = "rds"
  description = "rds or aurora - rds is cheaper, aurora is more available and scales better"
  type        = string
}

variable "rds_pg_instance_class" {
  default     = "db.t3.small"
  description = "The instance type of the RDS instance"
  type        = string
}

variable "rds_pg_allocated_storage" {
  default     = 5
  description = "RDS-only - allocated storage in gigabytes"
  type        = number
}

variable "aurora_pg_replica_scale_min" {
  default     = 1
  description = "Minimum number of reader nodes to create when replica_scale_enable is enabled"
  type        = number
}

variable "aurora_pg_replica_scale_max" {
  default     = 5
  description = "Maximum number of reader nodes to create when replica_scale_enable is enabled"
  type        = number
}

variable "aurora_pg_replica_scale_enabled" {
  default     = false
  description = "Enable to allow the aurora nodes to scale (with cpu load)"
  type        = bool
}

variable "aurora_pg_replica_count" {
  default     = 1
  description = "Number of reader nodes to create. If aurora_pg_replica_scale_enable is true, the value of aurora_pg_replica_scale_min is used instead."
  type        = number
}

variable "aurora_pg_instance_type" {
  default     = "db.r4.large"
  description = "Instance type to use at master instance. If instance_type_replica is not set it will use the same type for replica instances"
  type        = string
}

variable "aurora_pg_storage_encrypted" {
  default     = true
  description = "Specifies whether the underlying storage layer should be encrypted"
  type        = bool
}

variable "aurora_pg_apply_immediately" {
  default     = false
  description = "Determines whether or not any DB modifications are applied immediately, or during the maintenance window"
  type        = bool
}

variable "aurora_pg_monitoring_interval" {
  default     = 10
  description = "The interval (seconds) between points when Enhanced Monitoring metrics are collected"
  type        = number
}

variable "pg_port" {
  default     = "5432"
  description = "the postgress port to use"
  type        = string
}

variable "es_instance_type" {
  default     = "t2.small.elasticsearch"
  description = "the elasticsearch instance type"
  type        = string
}

variable "es_dedicated_master_type" {
  default     = "t2.small.elasticsearch"
  description = "the elasticsearch dedicated master instance type"
  type        = string
}

variable "es_version" {
  default     = 6.8
  description = "Version of Elasticsearch"
  type        = number
}

variable "es_instance_count" {
  default     = 3
  description = "how many elasticsearch instances to create - aka shard count"
  type        = number
}

variable "es_replica_count" {
  default     = 2
  description = "how many elasticsearch replicas of data there will be stored when writing"
  type        = number
}

variable "es_zone_awareness" {
  default     = true
  description = "Enable zone awareness for Elasticsearch cluster"
  type        = bool
}

variable "es_zone_awareness_count" {
  default     = 3
  description = "needs to be multiple of the instance_count (x1, x2, x3) if we set es_zone_awareness"
  type        = number
}

variable "es_create_iam_service_linked_role" {
  default     = true
  description = "Whether to create IAM service linked role for AWS ElasticSearch service. Can be only once per AWS account. KLUDGE."
  type        = bool
}

variable "es_ebs_volume_size" {
  default     = 35
  description = "the EBS volume size for the elasticsearch cluster - this aligns with the note times for the elasticsearch cluster"
  type        = number
}

variable "defaultorgname" {
  default     = ""
  description = "a default org name if one is desired"
  type        = string
}

variable "anywhere" {
  default     = "0.0.0.0/0"
  description = "CIDR range of anywhere"
  type        = string
}

variable "dns_zone" {
  description = "the zone in which we'll create the api end point using servicename var"
  type        = string
}

variable "cidr" {
  default     = "172.26.198.0/24"
  description = "CDIR range to be used for the VPC of the cinc deploy and dependant services (Postgres, ES)"
  type        = string
}

variable "cert_arn" {
  description = "AWS ARN to the certificate to be associated to the ALB to enable HTTPS connections"
  type        = string
}

variable "pg_username" {
  default     = "cincadmin"
  description = "master postgres username for the cinc server to use"
  type        = string
}

variable "pg_password" {
  default     = null
  description = "We'll generate a random one (stored in AWS secrets manager) if this is left null. Otherwise, the value set is used."
  type        = string
}

variable "ec2_pubkey" {
  description = "ssh keypair that must be supplied"
  type        = string
}

variable "custom_userdata_start" {
  default     = "#!/bin/bash -v"
  description = "additional userdata at the start of the run - a nice place to inject customizations"
  type        = string
}

variable "custom_userdata_end" {
  default     = "#!/bin/bash -v"
  description = "additional userdata at the end of the run - a nice place to inject customizations"
  type        = string
}

variable "cloudwatch_enabled" {
  default     = true
  description = "can be set to false if you need alternative logging - you can inject your own via custom_userdata variables."
  type        = bool
}

variable "cloudwatch_log_retention" {
  default     = 90
  description = "how long to retain logs in cloudwatch for"
  type        = number
}

variable "pushjobs_enabled" {
  default     = false
  description = "can be set to `true`, if you want to enable push-jobs networking. In its current state this will require your own code in one of the `custom_userdata_*` variables. Pushjobs will only run on the bootstrap node."
  type        = bool
}

variable "autoscaling_enabled" {
  default     = true
  description = "can be set to `false`, used to disable autoscaling if you don't want it. Recommend you leave it enabled!"
  type        = bool
}

variable "autoscaling_up_threshold" {
  default     = 60
  description = "sustained load over which we increase the instance count"
  type        = number
}

variable "autoscaling_down_threshold" {
  default     = 40
  description = "sustained load under which we decrease the instance count"
  type        = number
}

variable "adminkeys" {
  description = "additional keypairs"
  default     = ""
  type        = string
}

variable "adminranges" {
  description = "admin ranges for ssh access etc - make this as open as you need it to be"
  default     = ["192.168.0.0/24"]
  type        = list
}

variable "clientranges" {
  description = "client ranges for cinc/chef clients - make this as open as you need it to be"
  default     = ["192.168.0.0/24"]
  type        = list
}
