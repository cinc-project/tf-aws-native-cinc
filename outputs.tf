output "api_hostname" {
  value       = aws_route53_record.cinc.fqdn
  description = "point your API clients (cinc nodes, knife config) at this hostname"
}

data "aws_instances" "bootstrap_instances_meta" {
  instance_tags = {
    # Use whatever name you have given to your instances
    Name = "${var.servicename}-${var.environment}-bootstrap"
  }
}

output "bootstrap_ssh_line" {
  value       = "ssh ${local.ssh_user}@${data.aws_instances.bootstrap_instances_meta.public_ips[0]}"
  description = "a hint at how to log onto the bootstrap node, in order to create orgs and users - be sure to copy the keys off after they are created, nodes aren't immutable"
}
