
# EC2 Launch Configuration to create EC2 instances that run the CINC software
resource "aws_launch_configuration" "cinc_cluster" {
  name_prefix                 = "${var.servicename}-${var.environment}-"
  image_id                    = local.cinc_cluster_distro_ami
  instance_type               = var.ec2_instance_type
  key_name                    = aws_key_pair.accesskey.key_name
  security_groups             = [aws_security_group.cinc_cluster.id]
  iam_instance_profile        = aws_iam_instance_profile.cinc_cluster.name
  associate_public_ip_address = true
  enable_monitoring           = true
  user_data                   = data.template_cloudinit_config.cloudinit.rendered
  lifecycle {
    create_before_destroy = true
  }
}

# We tie together all the parts of the userdata / cloudinit here
data "template_cloudinit_config" "cloudinit" {
  gzip          = false # gzip because we're getting close to the limits of 16384
  base64_encode = false

  part {
    content_type = "text/x-shellscript"
    content      = var.custom_userdata_start
  }

  part {
    content_type = "text/x-shellscript"
    content      = local.adminkeys_userdata
  }

  part {
    content_type = "text/x-shellscript"
    content      = local.update_userdata
  }

  part {
    content_type = "text/x-shellscript"
    content      = local.cloudwatch_userdata
  }

  part {
    content_type = "text/x-shellscript"
    content      = local.elasticsearch_signingproxy_userdata
  }

  part {
    content_type = "text/x-shellscript"
    content      = local.pushjobs_userdata
  }

  part {
    content_type = "text/x-shellscript"
    content      = local.cinc_install_userdata
  }

  part {
    content_type = "text/x-shellscript"
    content      = var.custom_userdata_end
  }

}

# We determine if we're on RDS or Aurora style Postgres and set vars appropriately between them
locals {
  rds_host    = var.postgres_backend == "rds" ? module.pg_rds_master[0].this_db_instance_address : ""
  aurora_host = var.postgres_backend == "aurora" ? module.pg_aurora[0].this_rds_cluster_endpoint : ""
  pg_host     = coalesce(local.rds_host, local.aurora_host)
}

locals {
  rds_port    = var.postgres_backend == "rds" ? module.pg_rds_master[0].this_db_instance_port : ""
  aurora_port = var.postgres_backend == "aurora" ? module.pg_aurora[0].this_rds_cluster_port : ""
  pg_port     = coalesce(local.rds_port, local.aurora_port)
}

# we pull the non-secret username from the db module
locals {
  rds_username    = var.postgres_backend == "rds" ? module.pg_rds_master[0].this_db_instance_username : ""
  aurora_username = var.postgres_backend == "aurora" ? module.pg_aurora[0].this_rds_cluster_master_username : ""
  pg_username     = coalesce(local.rds_username, local.aurora_username)
}

locals {
  pg_password_arn = var.pg_password == null ? module.postgres_master_random_password[0].secret_arn : "NOTSET"
  var_pg_password = var.pg_password == null ? "NOTSET" : var.pg_password
}

# modular cloudwatch and pushjobs
locals {
  cloudwatch_userdata = var.cloudwatch_enabled == true ? data.template_file.cloudwatch_userdata[0].rendered : "#!/bin/bash -v"
}

locals {
  pushjobs_userdata = var.pushjobs_enabled == true ? data.template_file.pushjobs_userdata[0].rendered : "#!/bin/bash -v"
}

# picking distro ami
locals {
  cinc_cluster_distro_ami_ubuntu = var.cinc_cluster_distro_type == "ubuntu" ? data.aws_ami.ubuntu[0].id : ""
  cinc_cluster_distro_ami_debian = var.cinc_cluster_distro_type == "debian" ? data.aws_ami.debian[0].id : ""
  cinc_cluster_distro_ami_el     = var.cinc_cluster_distro_type == "el" ? data.aws_ami.el[0].id : ""
  cinc_cluster_distro_ami_amzn2  = var.cinc_cluster_distro_type == "amzn2" ? data.aws_ami.amzn2[0].id : ""
  cinc_cluster_distro_ami_sles   = var.cinc_cluster_distro_type == "sles" ? data.aws_ami.sles[0].id : ""
  cinc_cluster_distro_ami        = var.cinc_cluster_distro_ami == null ? coalesce(local.cinc_cluster_distro_ami_ubuntu, local.cinc_cluster_distro_ami_debian, local.cinc_cluster_distro_ami_el, local.cinc_cluster_distro_ami_amzn2, local.cinc_cluster_distro_ami_sles) : var.cinc_cluster_distro_ami
}

# ssh login system user (from: https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/managing-users.html)
locals {
  ssh_user_ubuntu = var.cinc_cluster_distro_type == "ubuntu" ? "ubuntu" : ""
  ssh_user_debian = var.cinc_cluster_distro_type == "debian" ? "admin" : ""
  ssh_user_el     = var.cinc_cluster_distro_type == "el" ? "ec2-user" : ""
  ssh_user_amzn2  = var.cinc_cluster_distro_type == "amzn2" ? "ec2-user" : ""
  ssh_user_sles   = var.cinc_cluster_distro_type == "sles" ? "ec2-user" : ""
  ssh_user        = var.ssh_user == null ? coalesce(local.ssh_user_ubuntu, local.ssh_user_debian, local.ssh_user_el, local.ssh_user_amzn2, local.ssh_user_sles) : var.ssh_user
}

# picking update and dependency install style
locals {
  update_userdata_ubuntu = var.cinc_cluster_distro_type == "ubuntu" ? local.update_apt_userdata : ""
  update_userdata_debian = var.cinc_cluster_distro_type == "debian" ? local.update_apt_userdata : ""
  update_userdata_el     = var.cinc_cluster_distro_type == "el" ? local.update_yum_userdata : ""
  update_userdata_amzn2  = var.cinc_cluster_distro_type == "amzn2" ? local.update_yum_userdata : ""
  update_userdata_sles   = var.cinc_cluster_distro_type == "sles" ? local.update_zipper_userdata : ""
  update_userdata        = var.update_userdata == null ? coalesce(local.update_userdata_ubuntu, local.update_userdata_debian, local.update_userdata_el, local.update_userdata_amzn2, local.update_userdata_sles) : var.update_userdata
}

# This should all be replaced by omnitruck installer.
# Work out things related to the package and finally, create a package url if one isn't specified. only properly tested on ubuntu so far.
locals {
  package_type_ubuntu = var.cinc_cluster_distro_type == "ubuntu" ? "deb" : ""
  package_type_debian = var.cinc_cluster_distro_type == "debian" ? "deb" : ""
  package_type_el     = var.cinc_cluster_distro_type == "el" ? "rpm" : ""
  package_type_amzn2  = var.cinc_cluster_distro_type == "amzn2" ? "rpm" : ""
  package_type_sles   = var.cinc_cluster_distro_type == "sles" ? "rpm" : ""
  package_type        = coalesce(local.package_type_ubuntu, local.package_type_debian, local.package_type_el, local.package_type_amzn2, local.package_type_sles)
  packager_cmd_ubuntu = var.cinc_cluster_distro_type == "ubuntu" ? "dpkg -i" : ""
  packager_cmd_debian = var.cinc_cluster_distro_type == "debian" ? "dpkg -i" : ""
  packager_cmd_el     = var.cinc_cluster_distro_type == "el" ? "rpm -i" : ""
  packager_cmd_amzn2  = var.cinc_cluster_distro_type == "amzn2" ? "rpm -i" : ""
  packager_cmd_sles   = var.cinc_cluster_distro_type == "sles" ? "rpm -i" : ""
  packager_cmd        = coalesce(local.packager_cmd_ubuntu, local.packager_cmd_debian, local.packager_cmd_el, local.packager_cmd_amzn2, local.packager_cmd_sles)
  # and set the url if it isn't set directly
  core_server_installer_base = "${var.cinc_mirror_base_path}${var.cinc_build_flavour}/cinc-server/${var.cinc_server_version}/${var.cinc_build_path}/"
  # and replace the local with that url if the var isn't set
  core_server_installer_url = var.core_server_installer_url == null ? "${var.cinc_mirror_base_path}${var.cinc_build_flavour}/cinc-server/${var.cinc_server_version}/${var.cinc_build_path}/cinc-server-core_${var.cinc_server_version}-1_amd64.${local.package_type}" : var.core_server_installer_url
}

locals {
  adminkeys_userdata = <<EOT
#!/bin/bash -v
cat >> ~${local.ssh_user}/.ssh/authorized_keys <<- EOF
${var.adminkeys}
EOF
EOT
}


locals {
  update_apt_userdata = <<EOT
#!/bin/bash -v
apt-get update
apt-get -y upgrade
apt-get -y install awscli python2.7 curl
EOT
}

locals {
  update_zipper_userdata = <<EOT
#!/bin/bash -v
zypper refresh
zypper --non-interactive update -y
zypper --non-interactive install -y aws-cli python2 curl
EOT
}

locals {
  update_yum_userdata = <<EOT
#!/bin/bash -v
yum update -y
yum install -y curl
yum search aws-cli && yum install -y aws-cli
python --version | grep "Python 2" || yum search python2.7 && yum install -y python2.7
EOT
}

locals {
  elasticsearch_signingproxy_userdata = <<EOT
#!/bin/bash -v
set -o errexit -o nounset -o pipefail

echo ">>> Installing aws-signing-proxy command"
curl -s -L https://github.com/chef-customers/aws-signing-proxy/releases/download/v0.5.0/aws-signing-proxy -o /usr/local/bin/aws-signing-proxy
chmod 755 /usr/local/bin/aws-signing-proxy

cat > /etc/systemd/system/aws-signing-proxy.service <<- EOF
[Unit]
Description=aws-signing-proxy
After=network.target
#
[Service]
ExecStart=/usr/local/bin/aws-signing-proxy
Restart=always
#
[Install]
WantedBy=multi-user.target
EOF

cat > /etc/aws-signing-proxy.yml <<- EOF
listen-address: 127.0.0.1
port: 9200
target: https://${module.es.endpoint}
region: ${var.aws_region}
no-file-log: false
stdout-log: true
EOF

systemctl daemon-reload && systemctl start aws-signing-proxy
EOT
}

locals {
  cinc_install_userdata = <<EOT
#!/bin/bash -v

mkdir -p /etc/opscode
mkdir -p /etc/chef-manage

cat > /etc/chef-manage/manage.rb <<- EOF
disable_sign_up = true
EOF

PG_PASSWORD_ARN=${local.pg_password_arn}

if [ "$PG_PASSWORD_ARN" == "NOTSET" ]; then
  # if var.pg_password was set we use the local to assign it
  DB_PASSWORD=${local.var_pg_password} # a dummy local value is used so that we can render this
else
  # otherwise we go with the ARN dynamically, keeping this entry out of the userdata
  DB_PASSWORD=`aws secretsmanager get-secret-value --region ${var.aws_region} --secret-id $PG_PASSWORD_ARN --query SecretString --output text`
fi

cat > /etc/opscode/chef-server.rb <<- EOF
api_fqdn = '${var.servicename}.${var.dns_zone}'.downcase
nginx['enable_non_ssl'] = true
postgresql['external'] = true
postgresql['vip'] = '${local.pg_host}'
postgresql['port'] = ${local.pg_port}
postgresql['db_superuser'] = '${local.pg_username}'
postgresql['db_superuser_password'] = '$DB_PASSWORD'
oc_chef_authz['http_init_count'] = 100
oc_chef_authz['http_queue_max'] = 200
opscode_erchef['authz_pooler_timeout'] = 2000
oc_bifrost['db_pool_init'] = 10
oc_bifrost['db_pool_max'] = 20
oc_bifrost['db_pool_queue_max'] = 40
opscode_erchef['depsolver_worker_count'] = 4
opscode_erchef['depsolver_timeout'] = 20000
opscode_erchef['db_pool_init'] = 10
opscode_erchef['db_pool_max'] = 100 # recommended based on load testing (thank tim ehlers)
opscode_erchef['db_pool_queue_max'] = 40
opscode_erchef['keygen_cache_workers'] = 2
opscode_erchef['keygen_cache_size'] = 100
opscode_erchef['nginx_bookshelf_caching'] = :on
opscode_erchef['s3_url_expiry_window_size'] = '100%'
opscode_erchef['s3_parallel_ops_fanout'] = 10
opscode_erchef['search_provider'] = 'elasticsearch'
opscode_erchef['search_queue_mode'] = 'batch'
opscode_solr4['external'] = true
opscode_solr4['external_url'] = 'http://localhost:9200'
opscode_solr4['elasticsearch_shard_count'] = ${var.es_instance_count}
opscode_solr4['elasticsearch_replica_count'] = ${var.es_replica_count}
bookshelf['storage_type'] = :sql
bookshelf['db_pool_init'] = 10
bookshelf['db_pool_max'] = 20
bookshelf['vip'] = '${var.servicename}.${var.dns_zone}'.downcase
rabbitmq['enable'] = false
rabbitmq['management_enabled'] = false
rabbitmq['queue_length_monitor_enabled'] = false
opscode_expander['enable'] = false
dark_launch['actions'] = false
default_orgname '${var.defaultorgname}'.empty? ? null : '${var.defaultorgname}'
EOF

cd /tmp/
echo ">>> Downloading and install package"

INSTALLER_URL_DIRECT=${var.core_server_installer_url}

if [ ! -z "$INSTALLER_URL_DIRECT" ]; then
  # We've probably got a direct url to the package desired.. try that
  curl -o cinc-server-core.${local.package_type} -OL $INSTALLER_URL_DIRECT
else
  # Otherwise, if the direct url var is empty we will try and find a package in the base mirror
  LINKS=`curl -s ${local.core_server_installer_base} | grep cinc-server-core | grep -Eo 'href="[^\"]+"' | cut -d '"' -f2`
  for LINK in $LINKS; do
    curl -s -o $LINK -OL ${local.core_server_installer_base}$LINK
  done
fi
${local.packager_cmd} cinc-server-core*.${local.package_type}

export STACKNAME="${var.servicename}-${var.environment}"
export BUCKET="${aws_s3_bucket.cinc_cluster_state.id}"
export AWS_REGION=${var.aws_region}
export PUBLIC_URL="https://${var.servicename}.${var.dns_zone}" #needed for the server reconfigure to work

aws s3 cp s3://$BUCKET/$STACKNAME-startup.sh startup.sh
chmod +x startup.sh
# we check the hash before running - this forces us to recreate the scaling group based on changes to startup.sh due to terraform recreating the launch config based on the updated md5 checked here
md5sum startup.sh | grep ${aws_s3_bucket_object.startup_sh.etag} && ./startup.sh
EOT
}
